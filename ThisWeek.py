# ComicRack Declarations
#
#@Name  This Week
#@Hook  CreateBookList
#@Key   ThisWeek
#@PCount 0

from datetime import datetime, date, timedelta
import clr
clr.AddReference('System.Windows.Forms')
from System.Windows.Forms import MessageBox
from System.IO import *

def ThisWeek(books, offset, b):
    comics = []
    today = date.today().isocalendar()
    try:
        for book in books:
            bookdate = datetime(book.AddedTime).isocalendar()
            if today[0] == bookdate[0] and today[1] == bookdate[1]:
                comics.append(book)

    except Exception, ex:
        print "Something went wrong"
        print ex

    return comics

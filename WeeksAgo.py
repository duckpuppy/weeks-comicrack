# ComicRack Declarations
#
#@Name  Weeks Ago
#@Hook  CreateBookList
#@Key   WeeksAgo
#@PCount 1

from datetime import datetime, date, timedelta
import clr
clr.AddReference('System.Windows.Forms')
from System.Windows.Forms import MessageBox
from System.IO import *

def WeeksAgo(books, offset, b):
    comics = []
    week = (date.today() - timedelta(weeks=int(offset))).isocalendar()
    try:
        for book in books:
            bookdate = datetime(book.AddedTime).isocalendar()
            if week[0] == bookdate[0] and week[1] == bookdate[1]:
                comics.append(book)

    except Exception, ex:
        print "Something went wrong"
        print ex

    return comics

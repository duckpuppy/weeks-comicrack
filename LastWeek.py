# ComicRack Declarations
#
#@Name  Last Week
#@Hook  CreateBookList
#@Key   LastWeek
#@PCount 0

from datetime import datetime, date, timedelta
import clr
clr.AddReference('System.Windows.Forms')
from System.Windows.Forms import MessageBox
from System.IO import *

def LastWeek(books, offset, b):
    comics = []
    lastweek = (date.today() - timedelta(weeks=1)).isocalendar()
    try:
        for book in books:
            bookdate = datetime(book.AddedTime).isocalendar()
            if lastweek[0] == bookdate[0] and lastweek[1] == bookdate[1]:
                comics.append(book)

    except Exception, ex:
        print "Something went wrong"
        print ex

    return comics
